"""ceg_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url, include

from book import views
from book.views import book_list_view, author_list_view, library_list_view

urlpatterns = [
    url(r'^/',  book_list_view, name="book_views"),
    url(r'^books/',  book_list_view, name="book_views"),
    url(r'^authors/',  author_list_view, name="author_views"),
    url(r'^library/',  library_list_view, name="library_views"),
    url(r'^api/', include('api.urls'), name="api"),

]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls))
    ] + urlpatterns
