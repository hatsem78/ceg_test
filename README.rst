=================
Pasos
=================

* Instalar requerimientos del proyecto
* Cargar datos ejecutando: python manage.py loaddata data.json



El proyecto es de un sistema donde se almacenan libros (Book) y sus autores(Author), ademas se tienen
librerias(Library) en las cuales estan disponibles esas esos libros.
Existen 3 vistas:
 - /book/:
    Se muestra la lista de libros, paginada, en una tabla con el titulo, autor y librerias en las que se encuntra.
    Se debe verificar y optimizar, si es posible.
 - /author/
    Se muestra la lista de autores, paginada, en una tabla con el nombre del autor y la cantidad de libros que escribio.
    Se debe verificar y optimizar, si es posible.
 - /library/
    Se muestra la lista de librerias, paginada, en una tabla con el nombre, la cantidad de libros que tiene y
    y la cantidad de autores que se pueden encontar.
    Se debe verificar y optimizar, si es posible.

Tambien se debe verificar los test y si hay alguno que no funcione, se debe arreglar y/o completar la suite de test.
