from rest_framework import serializers, generics
from django.db.models import F, Count
from api.utils import Pagination
from book.models import Library, Author



class LibrarySerializer(serializers.Serializer):
    id = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=200)
    count_author = serializers.CharField(max_length=10)
    count_library = serializers.CharField(max_length=10)

class LibraryList(generics.ListAPIView):
    queryset = Library.objects.filter(book__libraries__id=F('id')).annotate(count_library=Count("id"))\
        .annotate(count_author=Count("book__author_id", distinct=True)).order_by()
    serializer_class = LibrarySerializer
    pagination_class = Pagination



