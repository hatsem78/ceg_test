from unittest import TestCase

from rest_framework.test import RequestsClient

class LibraryTest(TestCase):
    def setUp(self):
        super(LibraryTest, self).setUp()

    def test_list(self):
        client = RequestsClient()
        response = client.get('http://localhost:8000/book/api/library/list/')
        assert response.status_code == 200