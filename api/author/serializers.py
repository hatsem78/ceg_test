from rest_framework import serializers, generics
from django.db.models import F, Count
from api.utils import Pagination
from book.models import Author


class AuthorSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=10)
    first_name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    count_book = serializers.CharField(max_length=10)



class AuthorList(generics.ListAPIView):
    queryset = Author.objects.filter(book__author_id=F('id')).values()\
                .annotate(count_book=Count("id")).order_by()

    serializer_class = AuthorSerializer
    pagination_class = Pagination