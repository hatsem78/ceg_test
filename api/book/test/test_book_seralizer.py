from unittest import TestCase

from rest_framework.test import RequestsClient

class BookTest(TestCase):
    def setUp(self):
        super(BookTest, self).setUp()

    def test_list(self):
        client = RequestsClient()
        response = client.get('http://localhost:8000/book/api/book/list/')
        assert response.status_code == 200