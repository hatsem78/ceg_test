from rest_framework import generics, serializers
from api.utils import Pagination
from book.models import Book


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'get_author_name', 'get_libreries')

class BookList(generics.ListAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    print(queryset.values())
    pagination_class = Pagination



