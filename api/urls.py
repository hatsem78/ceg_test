# coding=utf-8
from api.author.serializers import AuthorList
from api.book.serializers import BookList
from api.library.serializers import LibraryList
from django.conf.urls import url, include
from rest_framework import routers


router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'book/', BookList.as_view(), name='book_list'),
    url(r'library', LibraryList.as_view(), name='library_list'),
    url(r'author/', AuthorList.as_view(), name='author_list'),
    url(r'^api-auth', include('rest_framework.urls', namespace='rest_framework')),
]
