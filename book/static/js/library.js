Vue.component('vuetable', window.Vuetable.Vuetable);

Vue.component('vuetable-pagination', window.Vuetable.VuetablePagination);

Vue.component('inputFieldscustom', {
props: {
		rowData: {
			type: Object,
			required: true
		},
		field: {
			type: Object
		},
		rowIndex: {
			type: Number
		},
	},
    mounted(){

    },
	methods: {
    },
	template: `   		
	<div>
		{{ rowData.last_name }} {{ rowData.first_name }} 
	</div>
	`
});

new Vue({
    el: '#app',
    components: {
       'vuetable-pagination': Vuetable.VuetablePagination
    },
    data: {
        fields: [
            {
                name: 'name',
                title: ' Full Name',
                sortField: 'Library'
            },
            {
                name: 'count_library',
                title: 'Books in library',
                sortField: 'count_library'
            },
            {
                name: 'count_author',
                title: 'Different author quantity',
                sortField: 'count_author'
            }
        ],
        sortOrder: [
            { field: 'first_name', direction: 'asc' }
        ],
        css: {
        table: {
            tableClass: 'table table-striped table-bordered table-hovered',
            loadingClass: 'loading',
            ascendingIcon: 'glyphicon glyphicon-chevron-up',
            descendingIcon: 'glyphicon glyphicon-chevron-down',
            handleIcon: 'glyphicon glyphicon-menu-hamburger',
        },
        pagination: {
            infoClass: 'pull-left',
            wrapperClass: 'vuetable-pagination pull-right',
            activeClass: 'btn-primary',
            disabledClass: 'disabled',
            pageClass: 'btn btn-border',
            linkClass: 'btn btn-border',
            icons: {
              first: '',
              prev: '',
              next: '',
              last: '',
            },
        }
    }
    },
      computed:{
      /*httpOptions(){
        return {headers: {'Authorization': "my-token"}} //table props -> :http-options="httpOptions"
      },*/
    },
    methods: {
        onPaginationData: function(paginationData) {
            this.$refs.pagination.setPaginationData(paginationData)
        },
        onChangePage: function(page) {
            this.$refs.vuetable.changePage(page)
        },
        editRow: function(rowData){

        },
        deleteRow: function(rowData){
            alert("You clicked delete on"+ JSON.stringify(rowData))
        },
        onLoading: function() {

        },
        onLoaded:function () {

        }
    }
});
