from django.db import models


class Library(models.Model):
    class Meta:
        verbose_name = "name"
    name = models.CharField(max_length=100)


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey('book.Author', on_delete=models.CASCADE)
    libraries = models.ManyToManyField('book.Library')

    @property
    def get_libreries(self):
        return ", ".join(library.name for library in self.libraries.all())

    @property
    def get_author_name(self):
        return self.author.get_name

    def __str__(self):
        return "%s" % self.title



class Author(models.Model):

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    @property
    def get_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def __str__(self):
        return self.get_name()
