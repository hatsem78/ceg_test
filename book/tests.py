from django.contrib.auth.models import User
from django.test import TestCase, Client
from pyquery import PyQuery as pq

from book.models import Author, Book, Library


class AnimalTestCase(TestCase):

    def setUp(self):
        self.username = 'admin'
        self.password = 'admin'
        self.user = User.objects.create_user(self.username, password=self.password)
        self.author = Author.objects.create(first_name='Django', last_name='Test')
        self.books = [
            Book.objects.create(author=self.author, title='Awesome'),
            Book.objects.create(author=self.author, title='Zen')
        ]
        self.library = Library.objects.create(name='Django')
        self.client = Client()
        self.guest_client = Client()
        self.client.login(username=self.username, password=self.password)
        self.urls = ['/book/', '/author/', '/library/']
        self.urls_not_access = ['book', 'author', 'library']

    def test_views_cannot_accessible_by_guest_user(self):
        for url in self.urls_not_access:
            response = self.guest_client.get(url)
            self.assertNotEqual(200, response.status_code)

    def test_logged_user_can_access_to_views(self):
        for url in self.urls:
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

    def test_ensure_book_ordering(self):
        response = self.client.get(self.urls[0])

        dom = pq(response.content)

        tr = dom('table tbody tr')
        self.assertEqual(tr.length, len(self.books))
        td1 = dom('table tbody tr:first td:first').html()
        td2 = dom('table tbody tr:last td:first').html()

        self.assertLessEqual(td1, td2)
    def test_library_info(self):
        library = Library.objects.get(pk=1)
        print(library.serializable_value('name'))
        self.assertEqual(library.serializable_value('name'), 'Django')
