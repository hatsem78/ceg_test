from django.shortcuts import render

def BookListView(request):
    return render(
        request,
        'book/book_list.html'
    )


def AuthorListView(request):
    return render(
        request,
        'book/author_list.html'
    )


def LibraryListView(request):
    return render(
        request,
        'book/library_list.html'
    )


book_list_view = BookListView
author_list_view = AuthorListView
library_list_view = LibraryListView
